from rest_framework import serializers
from models import Diary, Category


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'category',
        )


class DiarySerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source='category', read_only=True)

    class Meta:
        model = Diary
        fields = (
            'id',
            'title',
            'content',
            'pub_date',
            'category_name',
        )
