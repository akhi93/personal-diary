from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt

from serializers import DiarySerializer
from .forms import AddDiaryForm
from .models import Diary
from rest_framework.response import Response

import json


def home(request):
    return render(request, "index.html")


@csrf_exempt
def add_diary(request):
    form = AddDiaryForm(request.POST or None)
    if form.is_valid():
        note = form.save()
        return send_note(note)

    return HttpResponse("0")


def send_note(note):
    return HttpResponse(json.dumps({
        'id': note.id,
        'title': note.title,
        'content': note.content,
        'pub_date': str(note.pub_date),
        'category_name': note.category.category,
    }))


@csrf_exempt
def edit_diary(request, pk):
    instance = get_object_or_404(Diary, id=pk)
    initial = {
        'category_name': instance.category.category
    }
    form = AddDiaryForm(request.POST or None, instance=instance, initial=initial)
    if form.is_valid():
        note = form.save()
        return send_note(note)

    return HttpResponse("0")


def delete_diary(request, pk):
    instance = get_object_or_404(Diary, id=pk)
    instance.delete()
    return HttpResponse()


def view_diary(request):
    queryset = Diary.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, "index2.html", context)
