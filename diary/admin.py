from django.contrib import admin

# Register your models here.
from diary.models import Category, Diary

admin.site.register(Category)
admin.site.register(Diary)
