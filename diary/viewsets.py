import time

from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response

from serializers import *


class DiaryViewSet(viewsets.ViewSet):
    def list_diary(self, request, *args, **kwargs):
        # time.sleep(5)
        queryset = Diary.objects.all()
        serializer = DiarySerializer(queryset, many=True)
        return Response(serializer.data)

    def single_note(self, request, pk, *args, **kwargs):
        note = get_object_or_404(Diary, id=pk)
        serializer = DiarySerializer(note)
        return Response(serializer.data)
