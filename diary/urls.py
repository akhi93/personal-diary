from django.conf.urls import url
from viewsets import DiaryViewSet

import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^add/', views.add_diary, name='add'),
    url(r'^edit/(?P<pk>\d+)/$', views.edit_diary, name='edit'),
    url(r'^delete/(?P<pk>\d+)/$', views.delete_diary, name='delete'),

    url(r'^details/', views.view_diary, name='details'),
    url(r'^rest/$', DiaryViewSet.as_view({
        'get': 'list_diary',
    })),
    url(r'^rest/(?P<pk>\d+)/$', DiaryViewSet.as_view({
        'get': 'single_note',
    })),
]
