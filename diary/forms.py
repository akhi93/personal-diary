from django import forms

from models import Category, Diary


class AddDiaryForm(forms.ModelForm):
    category_name = forms.CharField(label='Category')

    def save(self, commit=True):
        obj = forms.ModelForm.save(self, False)

        category_name = self.cleaned_data['category_name']
        category, created = Category.objects.get_or_create(category=category_name)
        if created is True:
            category.save()

        obj.category = category
        if commit is True:
            obj.save()

        return obj

    class Meta:
        model = Diary
        fields = [
            "title",
            "content",
            "pub_date",

            'category_name'
        ]
