/**
 * Created by AKHI1993 on 7/10/2016.
 */
'use strict';

eventsApp.controller('EventController',

    function EventController($scope, $http) {
        $scope.notes = [];
        $scope.editNote = null;
        $scope.viewNote = null;
        $scope.addNote = null;

        $http({
            method: 'GET',
            url: '/rest/?format=json'
        }).then(function successCallback(response) {
            $scope.notes = response.data;
        }, function errorCallback(response) {
        });

        $scope.editClicked = function (note) {
            $scope.editNote = note;
            $('.section.active').removeClass('active');
            setTimeout(function () {
                jQuery("#edit").addClass('active');
            }, 300);
        };

        $scope.viewClicked = function (note) {
            $scope.viewNote = note;
            $('.section.active').removeClass('active');
            setTimeout(function () {
                jQuery("#blog").addClass('active');
            }, 300);
        };

        $scope.saveClicked = function () {
            $http({
                method: 'POST',
                url: '/edit/' + $scope.editNote.id + "/",
                data: jQuery.param($scope.editNote),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function successCallback(response) {
                $scope.editNote = null;
                $('.section.active').removeClass('active');

                setTimeout(function () {
                    jQuery("#notes").addClass('active');
                }, 300);
            }, function errorCallback(response) {
            });
        };

        $scope.addClicked = function () {
            $http({
                method: 'POST',
                url: '/add/',
                data: jQuery.param($scope.addNote),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function successCallback(response) {
                $scope.notes.push(response.data);
                $scope.addNote = null;

                $('.section.active').removeClass('active');
                setTimeout(function () {
                    jQuery("#notes").addClass('active');
                }, 300);
            }, function errorCallback(response) {
            });
        };

        $scope.deleteClicked = function (index, note) {

            $http({
                method: 'GET',
                url: '/delete/' + note.id + "/",
            }).then(function successCallback(response) {
                $scope.notes.splice(index, 1);
                $('.section.active').removeClass('active');

                setTimeout(function () {
                    jQuery("#notes").addClass('active');
                }, 300);
            }, function errorCallback(response) {
            });

        }


    }
);