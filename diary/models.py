from __future__ import unicode_literals

from django.db import models


class Category(models.Model):
    category = models.CharField(max_length=255)

    def __unicode__(self):
        return self.category


class Diary(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    pub_date = models.DateTimeField('date published')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.title
